package nrw;

/**
 * <p>
 * Materialien zu den zentralen NRW-Abiturpruefungen im Fach Informatik ab 2017.
 * </p>
 * <p>
 * Generische Klasse nrw.Queue<ContentType>
 * </p>
 * <p>
 * Objekte der generischen Klasse nrw.Queue (Warteschlange) verwalten beliebige
 * Objekte vom Typ ContentType nach dem First-In-First-Out-Prinzip, d.h., das
 * zuerst abgelegte Objekt wird als erstes wieder entnommen. Alle Methoden haben
 * eine konstante Laufzeit, unabhaengig von der Anzahl der verwalteten Objekte.
 * </p>
 * 
 * @author Klaus Bovermann
 * @author Martin Weise (Anpassungen Dokumentation)
 * @version Generisch_02 2014-02-21
 */
public class Queue<ContentType> {
	
	/* --------- private innere Klasse -------------- */
	
	private class QueueNode {

		private ContentType content = null;
		private QueueNode nextNode = null;

		/**
		 * Ein neues Objekt vom Typ QueueNode<ContentType> wird erschaffen. 
		 * Der Inhalt wird per Parameter gesetzt. Der Verweis ist leer.
		 * 
		 * @param pContent
		 */
		public QueueNode(ContentType pContent) {
			content = pContent;
			nextNode = null;
		}

		/**
		 * Der Verweis wird auf das Objekt, das als Parameter uebergeben wird,
		 * gesetzt.
		 * 
		 * @param pNext
		 */
		public void setNext(QueueNode pNext) {
			nextNode = pNext;
		}
		
		/**
		 * 
		 * @return das Objekt, auf das der aktuelle Verweis zeigt.
		 */
		public QueueNode getNext() {
			return nextNode;
		}

		/**
		 * 
		 * @return das Inhaltsobjekt
		 */
		public ContentType getContent() {
			return content;
		}
	}
	
	/* --------- Ende der privaten inneren Klasse -------------- */
	
	private QueueNode head;
	private QueueNode tail;

	/**
	 * Eine leere Schlange wird erzeugt. 
	 * Objekte, die in dieser Schlange verwaltet werden, muessen vom Typ
	 * ContentType sein.
	 */
	public Queue() {
		head = null;
		tail = null;
	}

	/**
	 * Die Anfrage liefert den Wert true, wenn die Schlange keine Objekte enthaelt, 
	 * sonst liefert sie den Wert false.
	 * 
	 * @return true, falls die Schlange leer ist, sonst false
	 */
	public boolean isEmpty() {
		return head == null;
	}

	/**
	 * Das Objekt pContentType wird an die Schlange angehaengt. 
	 * Falls pContentType gleich null ist, bleibt die Schlange unveraendert.
	 * 
	 * @param pContent
	 *            das anzuhaengende Objekt vom Typ ContentType
	 */
	public void enqueue(ContentType pContent) {
		if (pContent != null) {
			QueueNode lNewNode = new QueueNode(pContent);
			if (this.isEmpty()) {
				head = lNewNode;
				tail = lNewNode;
			} else {
				tail.setNext(lNewNode);
				tail = lNewNode;
			}
		}
	}

	/**
	 * Das erste Objekt wird aus der Schlange entfernt. 
	 * Falls die Schlange leer ist, wird sie nicht veraendert.
	 */
	public void dequeue() {
		if (!this.isEmpty()) {
			head = head.getNext();
			if (this.isEmpty()) {
				head = null;
				tail = null;
			}
		}
	}

	/**
	 * Die Anfrage liefert das erste Objekt der Schlange. 
	 * Die Schlange bleibt unveraendert. 
	 * Falls die Schlange leer ist, wird null zurueckgegeben.
	 *
	 * @return das erste Objekt der Schlange (vom Typ ContentType) oder null,
	 *         falls die Schlange leer ist.
	 */
	public ContentType front() {
		if (this.isEmpty()) {
			return null;
		} else {
			return head.getContent();
		}
	}
}
