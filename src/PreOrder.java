import nrw.Edge;
import nrw.Graph;
import nrw.List;
import nrw.Vertex;

public class PreOrder {

    public void preOrder(Graph g, Vertex v) {
        v.setMark(true);

        List<Vertex> nachbarn = g.getNeighbours(v);
        nachbarn.toFirst();
        while (nachbarn.hasAccess()) {
            var nachbarKnoten = nachbarn.getContent();
            if (!nachbarKnoten.isMarked()) {
                preOrder(g, nachbarKnoten);
            }
            nachbarn.next();
        }
        System.out.println(v.getID());
    }



    public static void main(String[] args) {

        var graph = new Graph();

        var a = new Vertex("A");
        var b = new Vertex("B");
        var c = new Vertex("C");
        var d = new Vertex("D");
        var e = new Vertex("E");

        graph.addVertex(a);
        graph.addVertex(b);
        graph.addVertex(c);
        graph.addVertex(d);
        graph.addVertex(e);

        var ab = new Edge(a, b, 0.0);
        var bc = new Edge(b, c, 0.0);
        var ce = new Edge(c, e, 0.0);
        var bd = new Edge(b, d, 0.0);
        var de = new Edge(d, e, 0.0);
        var be = new Edge(b, e, 0.0);

        graph.addEdge(ab);
        graph.addEdge(bc);
        graph.addEdge(ce);
        graph.addEdge(bd);
        graph.addEdge(de);
        graph.addEdge(be);

        System.out.println("Starte PreOrder-Suche...");
        new PreOrder().preOrder(graph, a);
        System.out.println("... fertig");

    }
}