import nrw.Edge;
import nrw.Graph;
import nrw.List;
import nrw.Vertex;

public class PostOrder {

    public void postOrder(Graph g, Vertex v) {
        v.setMark(true);

        List<Vertex> nachbarn = g.getNeighbours(v);
        nachbarn.toFirst();
        while (nachbarn.hasAccess()) {
            var nachbarKnoten = nachbarn.getContent();
            if (!nachbarKnoten.isMarked()) {
                postOrder(g, nachbarKnoten);
            }
            nachbarn.next();
        }
        System.out.println(v.getID());
    }



    public static void main(String[] args) {

        var graph = new Graph();

        var a = new Vertex("A");
        var b = new Vertex("B");
        var c = new Vertex("C");
        var d = new Vertex("D");
        var e = new Vertex("E");
        var f = new Vertex("F");

        graph.addVertex(a);
        graph.addVertex(b);
        graph.addVertex(c);
        graph.addVertex(d);
        graph.addVertex(e);
        graph.addVertex(f);

        var ab = new Edge(a, b, 0.0);
        var ac = new Edge(a, c, 0.0);
        var cd = new Edge(c, d, 0.0);
        var de = new Edge(d, e, 0.0);
        var cf = new Edge(c, f, 0.0);

        graph.addEdge(ab);
        graph.addEdge(ac);
        graph.addEdge(cd);
        graph.addEdge(de);
        graph.addEdge(cf);

        System.out.println("Starte PostOrder-Suche...");
        new PostOrder().postOrder(graph, a);
        System.out.println("... fertig");

    }
}