import nrw.Edge;
import nrw.Graph;
import nrw.List;
import nrw.Queue;
import nrw.Vertex;


public class Breitensuche {

    public void breitensuche(Graph g, Vertex v) {
        var queue = new Queue<Vertex>();
        v.setMark(true);
        queue.enqueue(v);
        while (!queue.isEmpty()) {
            Vertex front = queue.front();
            List<Vertex> nachbarn = g.getNeighbours(front);
            nachbarn.toFirst();
            while (nachbarn.hasAccess()) {
                var vertex = nachbarn.getContent();
                if (!vertex.isMarked()) {
                    vertex.setMark(true);
                    queue.enqueue(vertex);
                }
                nachbarn.next();
            }
            System.out.println(front);
            queue.dequeue();
        }
    }

    public static void main(String[] args) {

        var graph = new Graph();

        var a = new Vertex("A");
        var b = new Vertex("B");
        var c = new Vertex("C");
        var d = new Vertex("D");
        var e = new Vertex("E");

        graph.addVertex(a);
        graph.addVertex(b);
        graph.addVertex(c);
        graph.addVertex(d);
        graph.addVertex(e);

        var ab = new Edge(a, b, 0.0);
        var bc = new Edge(b, c, 0.0);
        var ce = new Edge(c, e, 0.0);
        var bd = new Edge(b, d, 0.0);
        var de = new Edge(d, e, 0.0);
        var be = new Edge(b, e, 0.0);
        var cd = new Edge(c, d, 0.0);


        graph.addEdge(ab);
        graph.addEdge(bc);
        graph.addEdge(ce);
        graph.addEdge(bd);
        graph.addEdge(de);
        graph.addEdge(be);
        graph.addEdge(cd);

        System.out.println("Starte Breitensuche...");
        new Breitensuche().breitensuche(graph, c);
        System.out.println("... fertig");

    }
}